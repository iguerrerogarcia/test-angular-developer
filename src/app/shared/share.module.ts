import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NO_ERRORS_SCHEMA, NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
  ],
  providers: [
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule
  ],
  exports: [
  ],
  schemas: [NO_ERRORS_SCHEMA]

  })
export class ShareModule {

  static forRoot(): ModuleWithProviders<ShareModule> {
    return {
      ngModule: ShareModule,
      providers: []
    };
  }

}
