export class User {

  constructor(
    public name: string,
    public email: string,
    public weightInKg: number,
    public heightInCm: number,
    public imc: number
) {}
}
